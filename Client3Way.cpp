// ClientU.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <sstream>
#include <WS2tcpip.h>
#include <iostream>

#pragma comment(lib, "ws2_32.lib")

//Definisi Variable Umum
const int maxlength = 5000; //Maximum data yang dapat di kirim
char buffer[maxlength]; //Tempat untuk meletakkan data tersebut

						//Definisi type enumerisasi
enum Type {
	SYN, SYNACK, ACK, FIN, FINACK, DATA
};

//Definisi struct data
struct Data {
	int stats;
	int num;
};

int main() {
	//Inisialisasi
	int c_counter = 0;
	bool waiting = false;
	Type shake = Type::SYN;

	WSAData data;
	WSAStartup(MAKEWORD(2, 2), &data);

	//Membuat Socket
	SOCKET sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	//Identifikasi Server
	sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_port = htons(8888);
	inet_pton(AF_INET, "127.0.0.1", &server.sin_addr);

	int s_len = sizeof(server);

	std::cout << "-=Client=-\nEnter any key to start...\n";
	std::string initWaitC;
	std::cin >> initWaitC;

	std::cout << "\n\n--Client Start--\n\n";

	//Mengulang proses penerimaan dan pengiriman data
	while (true) {
		int _recv;
		std::stringstream ss;
		memset(buffer, '\0', maxlength);

		//Ketika sedang mengirim data
		if (shake != Type::DATA && !waiting) {
			Type ty;

			if (shake == Type::SYN) {
				ty = Type::SYN;
				std::cout << "Client mengirim SYN\n";
			}
			else if (shake == Type::ACK) {
				std::cout << "Client mengirim ACK\n";
				ty = Type::ACK;
				shake = Type::DATA;
			}

			ss << ty << "-0";
			sendto(sock, ss.str().c_str(), maxlength, 0, (SOCKADDR *)&server, s_len);

			waiting = true;
		}

		//Ketika menerima data
		if (_recv = recvfrom(sock, buffer, maxlength, 0, (SOCKADDR *)&server, &s_len)) {
			std::string str(buffer);
			std::string ty = str.substr(0, str.find("-"));
			std::string dt = str.substr(str.find("-") + 1);
			Data data;
			data.stats = stoi(ty);
			data.num = stoi(dt);
			if (data.stats == Type::SYN) {

			}
			else if (data.stats == Type::SYNACK) {
				shake = Type::ACK;
				std::cout << "menerima SYNACK...\n";

				waiting = false;
			}
			else if (data.stats == Type::ACK) {
			}
			else if (data.stats == Type::FIN) {
			}
			else if (data.stats == Type::FINACK) {
				std::cout << "menerima FINACK...\n";
				std::cout << "mengirim ACK...\n";
				ss << Type::ACK << "-0";
				sendto(sock, ss.str().c_str(), maxlength, 0, (SOCKADDR *)&server, s_len);
				break;
			}
			else if (data.stats == Type::DATA) {
				std::cout << "\nmenerima DATA: " << data.num;
				ss.str("");
				if (data.num < maxlength) {
					ss << Type::DATA << "-10";
					for (int i = 0; i < 5; i++) {
						sendto(sock, ss.str().c_str(), maxlength, 0, (SOCKADDR *)&server, s_len);
					}
				}
				else if (data.num == maxlength) {
					std::cout << "\nmengirim FIN...\n";
					ss << Type::FIN << "-0";
					sendto(sock, ss.str().c_str(), maxlength, 0, (SOCKADDR *)&server, s_len);
				}

			}
		}
	}

	//Membersihkan Socket
	closesocket(sock);
	WSACleanup();
	return 0;
}
